import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

// format a date from NgbDateStruct to ISO format (yyyy-mm-dd)
export function ngbDate(date: string): NgbDateStruct {
    let result = new NgbDate(0, 0, 0);
    if (date != null) {
        const yyyy = +date.substr(0, 4);
        const mm =  +date.substr(5, 2);
        const dd =  +date.substr(8, 2);
        result.year = yyyy;
        result.month = mm;
        result.day = dd;
    }
    return result;
}
