import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct';

// format a date from NgbDateStruct to ISO format (yyyy-mm-dd)
export function isoDateFormat(dateStruct: NgbDateStruct): string {
    const yyyy = '' + dateStruct.year;
    const mm = formatWithZeros(dateStruct.month, 2);
    const dd = formatWithZeros(dateStruct.day, 2);
    const result = yyyy + '-' + mm + '-' + dd;
    return result;
}

// format a number in nb zeros
function formatWithZeros(datePart: number, nbZeros: number): string {
    let zeros = '';
    for (let i = 0; i < nbZeros; i++)
        zeros += '0';
    let result = zeros + datePart;
    result = result.substring(result.length - nbZeros);
    return result;
}