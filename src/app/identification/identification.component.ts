import { Component, OnInit, Inject } from '@angular/core';
import { CredentialsModel } from '../model/credentials.model';
import { UserService } from '../service/user.service';
import { ConnexionDTO } from '../model/connexionDTO.model';
import { Prestataire } from '../model/prestataire.model';
import { Router } from '@angular/router';
import { AuthUserService } from '../service/auth-user.service';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

// TODO ajouter un guard Authentification

@Component({
  selector: 'app-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.css']
})
export class IdentificationComponent implements OnInit {

  credentials: CredentialsModel;
  public dataStorage:any[];

  errorMessage = '';

  constructor(private router: Router,
              private _userService: UserService,
              private authUserService: AuthUserService,
              @Inject(SESSION_STORAGE) private storage: WebStorageService) {
              }
            
  ngOnInit() {
    this.credentials = {
      identifier: '',
      password: ''
    };
  }

  onSubmit() {
    // identification user
    this._userService
      .connection(this.credentials.identifier, this.credentials.password)
      .subscribe( (data) => {
        this.retourConnection(data);
      });
  }

  retourConnection(connexionDTO: ConnexionDTO) {
      if (connexionDTO.connexionOK) {
        console.log(connexionDTO.clientDTO.nom + ' est identifié');
        if (connexionDTO.clientDTO.role === 'Prestataire') {
          // lecture des données prestataires
          const idPrestataire = connexionDTO.clientDTO.idUtilisateur;
          this._userService
          .getPrestataire(idPrestataire)
          .subscribe( (data) => {
            this.retourGetPrestataire(data);
          });
        } else {
          console.log(connexionDTO.clientDTO.nom + ' n\'est pas un prestataire (code '
                    + connexionDTO.clientDTO.role + ')');
          this.errorMessage = connexionDTO.clientDTO.prenom + ' ' + connexionDTO.clientDTO.nom + ' n\'est pas un prestataire';
        }
      } else {
        console.log('L\'identifiant ou le mot de passe est incorrect');
        this.errorMessage = 'L\'identifiant ou le mot de passe est incorrect';
      }
  }

  retourGetPrestataire(prestataire: Prestataire) {
    console.log(prestataire);
    console.log(prestataire.nom + ' ' + prestataire.description + ' ' + prestataire.id);

    // authentification utilisateur
    this.authUserService.setUserLoggedIn();

    // stocker l'id prestataire dans session
    this.storage.set('idPrestataire', prestataire.id);
    this.storage.set('prenomPrestataire', prestataire.prenom);
    this.storage.set('nomPrestataire', prestataire.nom);
   
    // navigation vers l'agenda 
    this.router.navigate(['agenda']);

    // autre façon plus up to date de transmettre des paramètres (à débugger)
    //this.router.navigate(['informations'], { state: {data: prestataire}});
  }
}
