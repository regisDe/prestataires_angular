export class Prestataire {
    idUtilisateur: number;
    id: number;
    nom: string;
    prenom: string;
    email: string;
    login: string;
    password: string;
    civilite: string;
    dateNaissance: string;
    adresse: string;
    codePostal: number;
    ville: string;
    description: string;
    telPortable: string;
    telFixe: string;
    telephone: string;
    tauxHoraire: number;
    photo: string;
    dateInscription: string;
    dateDesinscription: string;
    typePrestation: number;
    prestation: { id: number }
}
