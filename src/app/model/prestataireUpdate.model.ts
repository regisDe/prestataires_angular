export class PrestataireUpdate {
    id: number;
    nom: string;
    prenom: string;
    email: string;
    description: string;
    telephone: string;
    telPortable: string;
    telFixe: string;
    tauxHoraire: number;
    photo: string;
    dateInscription: string;
    dateDesinscription: string;
    typePrestation: number;
}
