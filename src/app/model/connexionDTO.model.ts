export interface ConnexionDTO {
    connexionOK: boolean;
    clientDTO: {
        idUtilisateur: number;
        nom: string;
        prenom: string;
        email: string;
        login: string;
        password: string;
        civilite: {
            idCivilite: number;
            libelle : string;
        }
        numTelPortable: string;
        numTelFixe: string;
        motifSuspension: string;
        adresse: string;
        ville: string;
        codePostal: number;
        role: string;
        dateNaissance: Date;
        dateInscription: Date;
        dateDesinscription: Date;
        dateDebutSuspension: Date;
        dateFinSuspension: Date;
    };
}