export interface AgendaEvent {
    id: number;
    eventId: number;
    prestataireId: number;
    title: string;
    start: Date;
    end: Date;
    lockedBy: string;
    color: string;
}