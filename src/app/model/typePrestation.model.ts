export interface TypePrestation {
    id: number;
    libelle: string;
    description: string;
    illustration: string;
}