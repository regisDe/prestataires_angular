export class User {
    nom: string;
    prenom: string;
    dateDeNaissance: Date;
    numTelPortable: string;
    numTelFixe: string;
    adresseMail: string;
    adresse: string;
    dateInscription: Date;
    dateDesinscription: Date;
    login: string;
    password: string;
    ville: string;
    codePostal: number;
    clientDTO: object;
    events: object[];
    messages: object[];
}