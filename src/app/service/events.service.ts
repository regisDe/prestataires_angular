import { Injectable } from '@angular/core';
import { AgendaEvent } from '../model/agendaEvent.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class EventsService {

  private _url: string = 'http://localhost:9890/WSAgenda/dates/all';

  private events: AgendaEvent[];

  constructor(private http: HttpClient ) { 
    this.events = [];
  }

  findAll(): Observable<AgendaEvent[]> {
    console.log("calling service ==> " + this._url);
    return this.http.get<AgendaEvent[]>(this._url);
  }

}
