import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConnexionDTO } from '../model/connexionDTO.model';
import { User } from '../model/user.model';
import { Prestataire } from '../model/prestataire.model';
import { TypePrestation } from '../model/typePrestation.model';
import { PrestataireUpdate } from '../model/prestataireUpdate.model';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  // TODO rendre paramétrables les url de web services
  // TODO https://devblogs.microsoft.com/premier-developer/angular-how-to-editable-config-files/

  // web service orchestrateur url
  private _url_orchestrateur = 'http://localhost:9490/Orchestrateur/client';

  // web service prestataire url
  private _url_prestataire = 'http://localhost:9190/WSPrestataires/rest/public';


  connection(identifier: string, password: string): Observable<ConnexionDTO> {
    return this.http.get<ConnexionDTO>(this._url_orchestrateur
                              + '/connexion'
                              + '/' + identifier
                              + '/' + password);
  }

  getPrestataire(id: number): Observable<Prestataire> {
    return this.http.get<Prestataire>(this._url_prestataire
                              + '/prestataire?idPrestataire=' + id);
  }

  getTypesPrestations(): Observable<TypePrestation[]> {
    return this.http.get<TypePrestation[]>(this._url_prestataire
                              + '/types_prestations');
  }

  inscriptionPrestataire(prestataire: Prestataire): Observable<Prestataire> {
    console.log('appel du web service ' + this._url_orchestrateur + '/inscriptionprestataire');
    console.log('inscriptionPrestataire ' + prestataire);
    return this.http.post<Prestataire>(this._url_orchestrateur
                              + '/inscriptionprestataire', prestataire);
  }

  updatePrestataire(prestataire: PrestataireUpdate): Observable<Prestataire> {
    console.log('appel du web service ' + this._url_prestataire + '/inscription');
    console.log('updatePrestataire ' + prestataire);
    return this.http.post<Prestataire>(this._url_prestataire
                              + '/inscription', prestataire);
  }
}
