import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../service/user.service';
import { Prestataire } from '../model/prestataire.model';
import { DatePipe } from '@angular/common';
import { TypePrestation } from '../model/typePrestation.model';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct';
import { isoDateFormat } from '../_helpers/isoDateFormat';
import { ngbDate } from '../_helpers/ngbDate';
import { PrestataireUpdate } from '../model/prestataireUpdate.model';
import { AuthUserService } from '../service/auth-user.service';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-informations',
  templateUrl: './informations.component.html',
  styleUrls: ['./informations.component.css']
})
export class InformationsComponent implements OnInit {

  errorMessage = '';
  public dataStorage:any[];

  idPrestataire: number = null;
  prestataire: Prestataire = new Prestataire();

  password2: string;
  // Angular DatePicker date type
  dateNaissance: NgbDateStruct;
  dateInscription: NgbDateStruct;
  dateDesinscription: NgbDateStruct;

  typesPrestations: TypePrestation[];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private _userService: UserService,
              private authUserService: AuthUserService,
              public datepipe: DatePipe,
              @Inject(SESSION_STORAGE) private storage: WebStorageService) {
              }

  ngOnInit() {
    // lecture des types de prestations
    console.log('appel de getTypesPrestations');
    this._userService
    .getTypesPrestations()
    .subscribe( (data) => {
      this.retourGetTypesPrestations(data);
    });

    // lecture des données prestataires en cas d'update
    this.idPrestataire = this.storage.get('idPrestataire');
    console.log('idPrestataire session : ' + this.idPrestataire);
    if (this.idPrestataire == null) {
      // création
      console.log('create prestataire');
    } else {
      // update
      console.log('update prestataire');
      console.log('lecture des informations du prestataire : ' + this.idPrestataire);
      this._userService
      .getPrestataire(this.idPrestataire)
      .subscribe( (data) => {
        this.retourGetPrestataire(data);
      });
    }
  }

  retourGetTypesPrestations(typesPrestations: TypePrestation[]) {
    this.typesPrestations = typesPrestations;
    //console.log('typesPrestations : ' + JSON.stringify(this.typesPrestations));
  }

  retourGetPrestataire(prestataire: Prestataire) {
    console.log('retourGetPrestataire : ' + prestataire.nom + ' ' + prestataire.description);
    console.log('prestataire : ' + JSON.stringify(prestataire));

    this.storage.set('nomPrestataire', prestataire.nom);
    this.storage.set('prenomPrestataire', prestataire.prenom);

    // chargement des données prestataires
    this.prestataire = prestataire;
    // mapping des données prestataires avec les input form
    // suivant inscription ou mise à jour
    if (prestataire.telPortable) {
      this.prestataire.telPortable = prestataire.telPortable;
      this.prestataire.telephone = prestataire.telPortable;
    }
    else {
      this.prestataire.telPortable = prestataire.telephone;
      this.prestataire.telephone = prestataire.telephone;
    }
    this.prestataire.typePrestation = prestataire.prestation.id; 
    // formatage des dates en NgbDateStruct depuis string Json
    if (this.prestataire.dateNaissance != null) {
      this.dateNaissance = ngbDate(this.prestataire.dateNaissance);
    }
    if (this.prestataire.dateInscription != null) {
      this.dateInscription = ngbDate(this.prestataire.dateInscription);
    }
    if (this.prestataire.dateDesinscription != null) {
      this.dateDesinscription = ngbDate(this.prestataire.dateDesinscription);
    } else {
      this.dateDesinscription = null;
    }
  }

  onSubmit() {

    // formatage des dates en string pour le Json
    if (this.prestataire.id == null)
      this.prestataire.dateNaissance = isoDateFormat(this.dateNaissance);
    this.prestataire.dateInscription = isoDateFormat(this.dateInscription);
    // date désinscription non gérée dans Kikatou
    // if (this.dateDesinscription != null) {
    //   this.prestataire.dateDesinscription = isoDateFormat(this.dateDesinscription);
    // } else {
    //   this.prestataire.dateDesinscription = null;
    // }

    if (this.idPrestataire == null) {

      console.log('prestataire pour inscription : ' + JSON.stringify(this.prestataire));

      // enregistrement informations prestataire en création
      this._userService
        .inscriptionPrestataire(this.prestataire)
        .subscribe( (data) => {
          this.retourInscriptionPrestataire(data);
      });

    } else {

      // enregistrement partie des informations prestataire en modification
      let prestataireUpdate = new PrestataireUpdate();

      prestataireUpdate.id = this.prestataire.id;
      prestataireUpdate.nom = this.prestataire.nom;
      prestataireUpdate.prenom = this.prestataire.prenom;
      prestataireUpdate.email = this.prestataire.email;
      prestataireUpdate.description = this.prestataire.description;
      if (this.prestataire.telPortable) {
        prestataireUpdate.telephone = this.prestataire.telPortable;
        prestataireUpdate.telPortable = this.prestataire.telPortable;
      } else {
        prestataireUpdate.telephone = this.prestataire.telephone;
        prestataireUpdate.telPortable = this.prestataire.telephone;
      }
      prestataireUpdate.tauxHoraire = this.prestataire.tauxHoraire;
      prestataireUpdate.photo = this.prestataire.photo;
      prestataireUpdate.dateInscription = this.prestataire.dateInscription;
      prestataireUpdate.dateDesinscription = this.prestataire.dateDesinscription;
      prestataireUpdate.typePrestation = this.prestataire.typePrestation;

      console.log('prestataireUpdate pour maj : ' + JSON.stringify(prestataireUpdate));

      this._userService
        .updatePrestataire(prestataireUpdate)
        .subscribe( (data) => {
          this.retourUpdatePrestataire(data);
      });

    }

  }

  retourInscriptionPrestataire(prestataire: Prestataire) {
    console.log('prestataire ' + prestataire.id + ' créé : ' + prestataire.nom);
    // authentification utilisateur supprimée
    this.authUserService.setUserLoggedOut();
    // suppression en session de l'id prestataire
    this.storage.remove('idPrestataire');
    // navigation vers home
    this.router.navigate(['home']);
  }

  retourUpdatePrestataire(prestataire: Prestataire) {
    console.log('prestataire ' + prestataire.id + ' mis à jour : ' + prestataire.nom);
    this.router.navigate(['agenda']);
  }

}
