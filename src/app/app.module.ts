import { registerLocaleData } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import localeFr from '@angular/common/locales/fr';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IdentificationComponent } from './identification/identification.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './service/user.service';
import { EventsService } from './service/events.service';
import { InformationsComponent } from './informations/informations.component';
import { NgbModule, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { MustMatchDirective } from './_helpers/must-match.directive';
import { ForbiddenValidatorDirective } from './_helpers/forbidden-name.directive';
import { AgendaComponent } from './agenda/agenda.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { AuthUserService } from './service/auth-user.service';
import { AuthGuard } from './auth.guard';
import { StorageServiceModule } from 'angular-webstorage-service';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    IdentificationComponent,
    PageNotFoundComponent,
    InformationsComponent,
    MustMatchDirective,
    ForbiddenValidatorDirective,
    AgendaComponent
  ],
  imports: [
    CommonModule,
    NgbModalModule,
    FlatpickrModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    StorageServiceModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [UserService,
              EventsService,
              NgbDatepicker,
              DatePipe,
              AuthUserService,
              AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
