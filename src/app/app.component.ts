import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './service/user.service';
import { AuthUserService } from './service/auth-user.service';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'prestataires-app';

  idPrestataire: number = null;
  nomPrestataire: string;
  prenomPrestataire: string;
 
  constructor(private router: Router,
              private _userService: UserService,
              private authUserService: AuthUserService,
              @Inject(SESSION_STORAGE) private storage: WebStorageService) {
  }

  ngOnInit() {
    // lecture des données prestataires
    this.refreshInfosPrestataire();
  }

  refreshInfosPrestataire() {
    // lecture des données prestataires
    this.nomPrestataire = this.storage.get('nomPrestataire');
    this.prenomPrestataire = this.storage.get('prenomPrestataire');
  }

  navHome() {
    // lecture des données prestataires
    this.refreshInfosPrestataire();
    // navigation vers home
    this.router.navigate(['home']);
  }

  navAgenda() {
    // lecture des données prestataires
    this.refreshInfosPrestataire();
    // navigation vers agenda
    this.router.navigate(['agenda']);
  }

  navInformations() {
    // lecture des données prestataires
    this.refreshInfosPrestataire();
    // navigation vers informations
    this.router.navigate(['informations']);
  }

  deconnexion() {
    // authentification utilisateur supprimée
    this.authUserService.setUserLoggedOut();
    // suppression en session de l'id prestataire
    this.storage.remove('idPrestataire');
    this.idPrestataire = null;
    this.storage.remove('nomPrestataire');
    this.storage.remove('prenomPrestataire');
    // lecture des données prestataires
    this.refreshInfosPrestataire();
    // navigation vers home
    this.router.navigate(['home']);
  }

}
