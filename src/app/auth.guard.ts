import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthUserService } from './service/auth-user.service';
import { UserService } from './service/user.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authUserService: AuthUserService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authUserService.getUserLoggedIn();
  }

}
