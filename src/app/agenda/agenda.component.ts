import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { EventsService } from '../service/events.service';
import { AgendaEvent } from '../model/agendaEvent.model';

import {
  CalendarDateFormatter,
  CalendarEvent,
  CalendarView,
  DAYS_OF_WEEK,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';
import { CustomDateFormatter } from '../utils/custom-date-formatter.provider';

import { setHours, setMinutes, addDays } from 'date-fns';
import { colors } from '../utils/colors';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-agenda',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})

export class AgendaComponent implements OnInit {

  view: CalendarView = CalendarView.Month;

  viewDate = new Date();

  events: CalendarEvent[] = [];
 
  locale: string = 'fr';

  weekStartsOn: number = DAYS_OF_WEEK.MONDAY;

  weekendDays: number[] = [DAYS_OF_WEEK.FRIDAY, DAYS_OF_WEEK.SATURDAY];

  CalendarView = CalendarView;

  setView(view: CalendarView) {
    this.view = view;
  }

  refresh: Subject<any> = new Subject();
  
  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.refresh.next();
  }

  addEvent(date: Date): void {
    this.events.push({
      start: date,
      title: 'Je ne suis pas disponible',
      color: colors.red
    });
    this.refresh.next();
  }

  private myevents: AgendaEvent[];
  private idPrestataire =  +this.route.snapshot.paramMap.get('idPrestataire');

  constructor(private _eventsService: EventsService,
              private route: ActivatedRoute) { 
  }

  ngOnInit() {
    this.fetchEvents();
  }

  fetchEvents(): void {

    // lecture de la liste des événements d'agenda via le WS Agenda
    this._eventsService.findAll()
    .subscribe( (data) => { 
      this.myevents = data ; 
      // transfert des dates de myevents vers events
      for (let i = 0 ; i < this.myevents.length ; i++) {
        var myevent: AgendaEvent = this.myevents[i];
        if (myevent.prestataireId == this.idPrestataire )
         this.events.push({
          start: new Date(myevent.start),
          title: myevent.lockedBy,
          end: new Date(myevent.end),
          color: colors.yellow,
          draggable: true,
          resizable: {
            beforeStart: true, // this allows you to configure the sides the event is resizable from
            afterEnd: true
          }
        });
      }
      this.refresh.next();
    }
  );

}

refreshView(): void {
 // this.cssClass = this.cssClass === RED_CELL ? BLUE_CELL : RED_CELL;
  this.refresh.next();
}

}
