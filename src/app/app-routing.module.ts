import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IdentificationComponent } from "./identification/identification.component";
import { AgendaComponent } from './agenda/agenda.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InformationsComponent } from './informations/informations.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: IdentificationComponent },
  { path: 'informations', component: InformationsComponent },
  { path: 'agenda', canActivate: [AuthGuard], component: AgendaComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
